---
title: "Wireless Auditing and Monitoring Tools"
date: 2018-08-15 18:30:00
meeting_number: 64
youtube: https://www.youtube.com/watch?v=q4-GoS7o6mo
---
This week, Clif will be showing some cool tools for wireless monitoring and auditing.

## Weekly Content
* [Faxploit](https://thehackernews.com/2018/08/hack-printer-fax-machine.html)
* [Man-in-the-Disk](https://thehackernews.com/2018/08/man-in-the-disk-android-hack.html)
* [Cashout Blitz](https://krebsonsecurity.com/2018/08/fbi-warns-of-unlimited-atm-cashout-blitz/)

### Recommended Readings
* <https://thehackernews.com/2018/08/artificial-intelligence-malware.html>
* <https://securelist.com/keypass-ransomware/87412/>
* <https://www.welivesecurity.com/2018/08/13/cramming-code-bugs-secure/>
* <https://securelist.com/apt-trends-report-q2-2018/86487/>
* <https://thehackernews.com/2018/08/google-mobile-location-tracking.html>
* <https://thehackernews.com/2018/08/macos-mouse-click-hack.html>
* <https://www.darkreading.com/vulnerabilities---threats/hacker-unlocks-god-mode-and-shares-the-key/d/d-id/1332543>
* <https://thehackernews.com/2018/08/android-app-hack.html>
* <https://www.darkreading.com/risk/flaws-in-mobile-point-of-sale-readers-displayed-at-black-hat/d/d-id/1332555>
* <https://www.uc.edu/hr/ssc/training-resources/online-with-lynda.html>
