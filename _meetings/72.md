---
title: "Firewalls and IPTables"
date: 2018-10-09 18:30:00
meeting_number: 72
youtube: https://www.youtube.com/watch?v=8ddGDnmW2G4
---
This week, we talked about firewalls and network security and covered tools such as iptables and nmap.

## Weekly Content
* [Google+ shutting down](https://thehackernews.com/2018/10/google-plus-shutdown.html)
* [Silk Road admin pleads guilty](https://thehackernews.com/2018/10/silkroad-admin-gary-davis.html)
* [MicroTik router vulnerability resurfaces](https://thehackernews.com/2018/10/router-hacking-exploit.html)
* Recommended reading
    * <https://krebsonsecurity.com/2018/10/when-security-researchers-pose-as-cybercrooks-who-can-tell-the-difference/>
    * <https://www.welivesecurity.com/2018/10/05/virus-bulletin-2018-supply-chain-hacking-grows/>
