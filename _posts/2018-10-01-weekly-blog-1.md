---
title: "Weekly Blog: US Bank Tour"
date: 2018-10-01 18:43:58
author: young2r2
main_image:
  src: 2018-08-28-usbank-tour-group.jpg
  alt: Cyber@UC group photo with US Bank employees
  caption: >
    Cyber@UC at US Bank's Cyber Security Center
---
This week Cyber@UC was invited to a US Bank Security Operations tour. They showed us around their operations and what day-to-day security work looked like. They also showed us how they handled security incidents.

There are multiple levels of escalation that an incident can be analyzed. The lowest level of escalation is the first time suspicious traffic is discovered. This traffic needs further analysis by someone more specialized that may know more about the anomaly. The incident continues to escalate until it is resolved. The incident can be resolve from patch updates in software or additional network rules.

We spoke with many of the employees of US Bank's Security Operations. We told them about our organization's mission and were very happy to hear about what we've done so far about spreading good security practices and knowledge. They want to connect with us in the future and perhaps aid us in our own Security Operations Center.

## Lab Progress
This week in the lab, we have made some good progress. We have setup the network firewalls! Now we can connect to the rack machines remotely if we wish. We are now in the process of installing OpenStack on the Dell R730s. This step may take a while a need all the help we can get. Progress is good.
