---
title: "Security Operations Center specifications"
date: 2018-08-31 19:50:20
author: cardaraj
---
A security operations center is the lifeblood of cyber defense operations. It performs the critical function of collecting logs from hosts on the network, correlating events, filtering out noise, enriching events, storing events, and displaying events in an efficient format for analyzing them. Our SOC will use _only_ open source and free software, along with code we will write to glue everything together.

## Infrastructure

### Server rack configurations
We have four large server racks for full length servers and three smaller racks for half length servers. Only one of our smaller racks is in use. Out of the four larger racks there is one that is shorter and mobile; the function of this rack is TBD.

Two of the larger racks are identical and serve as our "sim" racks. These will be used for simulating red vs. blue missions, corporate network environments, and much more. The third rack is  our "augmentation" rack, which will be used to augment the function and computing power of the other two sim racks. The Augmentation rack will also be running our monitoring software for the SOC setup.

All our large racks are ERC 513, our server room.

**For our larger racks:**
* The sim racks contain the following hardware:
    - 6x Precision 7920
    - 2x PowerEdge R730
    - 1x Cisco ASA 5516-X
    - 1x Cisco SG550X-48P
    - 1x PSU
* The augmentation rack contains the following hardware:
    - 4x Precision 7920
    - 2x PowerEdge R730xd
    - 1x PowerEdge R720xd
* The shorter full length rack contain the following hardware:
    - 2x R730xd

As mentioned earlier there is only one small rack in use, it is in our main lab area ERC 513.  It functions as our labs network distribution point.  The main firewall and switch sit here.

**For our smaller racks:**
* The network entry rack contains the following hardware:
    - 1x Cisco ASA 5516-X
    - 1x Cisco 16P Switch

### Lab desktop computer setup
In our main lab area we have:
* 8x Dell Optiplex 7050 Workstations
* 8x Dell monitor E2417H
* 2x Dell 70 inch Conference Room Monitor    

The 70 inch monitors still need to be mounted on the walls. Our eight desktop computers are named after Snow White and the 7 Dwarves to distinguish them. These will function as our workstations for remote configuration and setup for our servers.

## Implementing the security operations center

This section will specify some of the software that our SOC will utilize.

### Apache Metron
{% include image.html src="2018-08-31-apache-metron.png" alt="Apache Metron diagram" %}

Apache Metron is the architecture that will be loosely followed for the SOC. A large focus of this project is finding areas of improvement in the software we use.

Metron takes into account host event logs, anti-virus logs, firewall logs, IDS logs (intrusion detection system), PCAPs (Packet Captures) and can be modified to process even more types of logs. The goal is to correlate and enrich these logs with context so proper detections can be made for them.  

Enrichment is done by comparing the collected logs with OSINT (Open Source Intelligence). OSINT is typically composed of IOCs (Indicators Of Compromise). If enough IOCs are seen in certain log events, you can be more confident that an event is malicious. OSINT can also take the form of behavioral patterns, in that certain behaviors, when repeated or combined, can be a strong indicator of a compromise.

### OpenStack
{% include image.html src="2018-08-31-openstack.png" alt="OpenStack diagram" %}

OpenStack is the platform we will use to build everything else on top of. In simple terms this software will allow us to have our own cluster of servers to spin up VMs (Virtual Machines). These VMs will be running software like Kafka, Flink, LogStash, Elasticsearch, Kibana, Moloch, and others needed to build up and improve the Metron architecture.

### Puppet
{% include image.html src="2018-08-31-puppet.jpg" alt="Puppet diagram" %}

Puppet will be used to manage the configurations and software installed on VMs in our OpenStack cloud. Puppet can push and run Bash scripts, enforce the settings in config files, and monitor all machines with Puppet installed. If a puppetized file is deleted on a VM, Puppet will notice and replace it again.

### FOG Project
{% include image.html src="2018-08-31-fog.gif" alt="FOG diagram" %}

FOG will be used to image our physical workstations with OSes to collect logs from. We can standardize an image with or without vulnerabilities and deploy it at scale to all lab desktops. It relies on PXE booting, a technology which allows for the remote imaging of computers.

### ELK stack
{% include image.html src="2018-08-31-elk.jpg" alt="ELK Stack diagram" %}

ELK (Elasticsearch, LogStash, Kibana) are three separate applications that can be easily combined to collect (LogStash), store (Elasticsearch), forward (LogStash), visualize (Kibana), and apply logic (Kibana/Elasticsearch) to data. The ELK stack is helpful for searching through large sets of data, otherwise known as threat hunting in a SOC context.

## Want to help?
Get in touch via email: [cardaraj@mail.uc.edu](mailto:cardaraj@mail.uc.edu)
